class User < ApplicationRecord
    
    # has_secure_password
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
        :recoverable, :rememberable, :trackable, :validatable

    mount_uploader :avatar, AvatarUploader


	ROLES = %w[moderator admin superadmin]
	def role?(base_role)
	  ROLES.index(base_role.to_s) <= ROLES.index(role)
	end

	def admin?
		role == "admin"
	end
end
