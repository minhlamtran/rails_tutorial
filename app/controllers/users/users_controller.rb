class Users::UsersController < ActionController::Base	
	before_action :authenticate_user!
	layout "application"
	def index	
		@user = current_user	
	end

	def show
	   # @user = User.find(params[:id])
	   @user = current_user
	   
		if @user.avatar_url
	   		@avatar_url = @user.avatar_url
	   	else
	   		@avatar_url = "image-test.img"
		end
	end

end