class ArticlesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]
  load_and_authorize_resource

  def index
  	@articles = Article.all
  end

  def new
  	@articles = Article.new
  	authorize! :manage, @article
  end

  def edit
    @article = Article.find(params[:id])
    authorize! :manage, @article
  end

  def show
  	@article = Article.find(params[:id])
  	authorize! :manage, @article
  end

  def create
  	@article = Article.new(article_params)
  	if @article.save
  		redirect_to @article
  	else
      render :new
  	end

  	authorize! :manage, @article
  end

  def update
    @article = Article.find(params[:id])
    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
    authorize! :manage, @article
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to articles_path
    authorize! :manage, @article
  end

  private
  def article_params
  	params.require(:article).permit(:title, :content)
  end
end
