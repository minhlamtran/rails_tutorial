class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
  
  check_authorization :unless => :devise_controller?

  #redirect_to my profile after login
  def after_sign_in_path_for(resource)
    "/profile/"
  end

end
