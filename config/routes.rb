Rails.application.routes.draw do

  get 'admins/index'

  	root 'home#index'
  
	devise_for :users, :controllers => {:registrations => "users/registrations"}
	# resources :users
	# match 'users/:id', to: 'users/users#show', via: [:get, :post]
	# match 'users/:id', to: 'users/users#index', via: [:get, :post]
	match 'profile/', to: 'users/users#index', via: [:get]
	match 'users/', to: 'users/users#show', via: [:get]

	# match 'users/sign_out' => "devise/sessions#destroy", via: [:delete]
	# delete 'users/sign_out'  => "devise/sessions#destroy"

	# devise_for :users do
	  # delete '/users/sign_out' => 'devise/sessions#destroy'
	# end

	resources :articles
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

 	
end
